'use strict';

/* http://docs.angularjs.org/guide/dev_guide.e2e-testing */

describe('Signup view', function() {

  beforeEach(function() {
    browser.get( '/#/signup' );
  });

  it( 'has appropriate title', function(){
    var centralLogo = element( by.css('.heading') );
    expect( centralLogo.getText() ).toEqual('Timezone management system');
  });

  it('informs about the possibility to Log in', function (  ) {
    var formWrapper = $('.form-wrapper');
    expect( formWrapper.getText() ).toContain("Already have an account?");
    expect( formWrapper.getText() ).toContain("Log in!");
  });

  it('contains input for login', function (  ) {
    var loginInput = $('#login_value');
    expect( loginInput ).toBeDefined();
  });

  it('contains input for password', function (  ) {
    var passInput = $('#pass_value');
    expect( passInput ).toBeDefined();
  });

  it('contains input for repeating password', function (  ) {
    var passInput = $('#pass_value2');
    expect( passInput ).toBeDefined();
  });

  it('contains input for first name', function (  ) {
    var firstNameInput = $('#first_name_value');
    expect( firstNameInput ).toBeDefined();
  });

  it('contains input for second name', function (  ) {
    var secondNameInput = $('#last_name_value');
    expect( secondNameInput ).toBeDefined();
  });

  it('contains a submit button', function (  ) {
    var submitBtn = $("input[type='submit'");
    expect( submitBtn ).toBeDefined();
  });

});