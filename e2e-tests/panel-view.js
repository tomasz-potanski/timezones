
describe('Panel view', function() {

  it('User should be able to create an account', function (  ) {
    browser.get( '/#/signup' );
    $('#login_value' ).sendKeys( 'Mr_Protractor' + Math.floor( (Math.random() * 1000) + 1 ) );
    $('#first_name_value' ).sendKeys( 'Eugeniusz' );
    $('#last_name_value' ).sendKeys( 'Wisniewski' );
    $('#pass_value' ).sendKeys('123456');
    $('#pass_value2' ).sendKeys('123456');

    var submitBtn = $("input[type='submit'");
    submitBtn.click();
  });

  it('User is redirected to panel', function (  ) {
    browser.getLocationAbsUrl().then( function ( url ) {
      expect( url ).toEqual( '/panel' );
    });
  });

  it( 'has appropriate title', function(){
    var centralLogo = element( by.css('.heading') );
    expect( centralLogo.getText() ).toEqual('Timezone management system');
  });
  
  it('Has input for filtering timezones', function (  ) {
      expect( $('#tzNameFilter') ).toBeDefined();
  });
  
  it('Gives the possibility to log out', function (  ) {
      expect( $('.top-right-management' ).getText() ).toContain( 'Log out' );
  });

  it('Gives the possibility to create a new timezone', function (  ) {
    expect( $('.top-right-management').getText() ).toContain( 'Create a new timezone' );
  });

  it('Gives the possibility to edit timezones', function (  ) {
    expect( $('.top-right-management').getText() ).toContain( 'Edit timezones' );
  });

  it('Has valid new timezone dialog, allowing to create a new one', function (  ) {
    $('#showNewTimezoneDialog' ).click();
    expect( $('.new-timezone-dialog .header' ).getText() ).toEqual('Create a new timezone');

    var timezoneNameInput = $('#name_value');
    expect( timezoneNameInput ).toBeDefined();
    timezoneNameInput.sendKeys( "Protractor's #" + Math.floor( (Math.random() * 1000) + 1 ) );

    var timezoneDiffInput = $('#diff_value');
    expect( timezoneDiffInput ).toBeDefined();
    timezoneDiffInput.sendKeys( "1" );

    var timezoneCityInput = $('#city_value');
    expect( timezoneCityInput ).toBeDefined();
    timezoneCityInput.sendKeys( "Barcelona" );

    var submitBtn = $('#validateAndCreateTimezone');
    expect( submitBtn ).toBeDefined();
    submitBtn.click();

    var timezoneBoxed = $$('.timezone-box');
    expect( timezoneBoxed.count() ).toEqual( 1 );
    expect( $('.timezone-box .city' ).getText() ).toEqual( "Barcelona" );
  });

  it('Filter functionality is working well', function (  ) {
    var filterInput = $('#tzNameFilter' );
    filterInput.sendKeys('Alabama');
    var timezoneBoxed = $$('.timezone-box');
    expect( timezoneBoxed.count() ).toEqual( 0 );

    filterInput.clear();
    expect( timezoneBoxed.count() ).toEqual( 1 );
  });

  it('Gives the possibility to change timezone data', function (  ) {
    $('#editTimezones' ).click();
    $('.timezone-box .edit-btn' ).click();

    var editDialog = $('.edit-timezone-dialog');
    expect( $('.edit-timezone-dialog .header' ).getText() ).toEqual( "Edit a timezone" );

    var nameInput = $('#name_value_edit' );
    expect( nameInput ).toBeDefined();

    var diffInput = $('#diff_value_edit');
    expect( diffInput ).toBeDefined();

    var cityInput = $('#city_value_edit');
    expect( cityInput ).toBeDefined();
    cityInput.clear();
    cityInput.sendKeys('Poznan');

    $('#validateAndUpdateTimezone' ).click();
    expect( $('.timezone-box .city' ).getText() ).toEqual( "Poznan" );
  });

  it('Gives the possibility to remove just created timezone', function (  ) {
    $( '.timezone-box .delete-btn' ).click();

    var timezoneBoxed = $$('.timezone-box');
    expect( timezoneBoxed.count() ).toEqual( 0 );
  });
  

});
