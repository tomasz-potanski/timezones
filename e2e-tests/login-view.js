'use strict';

/* http://docs.angularjs.org/guide/dev_guide.e2e-testing */

describe('Login view', function() {

  beforeEach(function() {
    browser.get( '/' );
  });

  it( 'should redirect #/login', function () {
    browser.getLocationAbsUrl().then( function ( url ) {
      expect( url ).toEqual( '/login' );
    });
  });

  it( 'has appropriate title', function(){
    var centralLogo = element( by.css('.heading') );
    expect( centralLogo.getText() ).toEqual('Timezone management system');
  });

  it('informs the user about using cookies', function (  ) {
      var cookieContainer = $('.cookie-info .text');
      expect( cookieContainer.getText() ).toEqual('Site uses cookie.')
  });

  it('informs about the possibility to Sign up', function (  ) {
      var formWrapper = $('.form-wrapper');
      expect( formWrapper.getText() ).toContain("Do not have an account yet?");
      expect( formWrapper.getText() ).toContain("Sign up!");
  });

  it('contains input for login', function (  ) {
      var loginInput = $('#login_value');
      expect( loginInput ).toBeDefined();
  });

  it('contains input for password', function (  ) {
      var passInput = $('#pass_value');
      expect( passInput ).toBeDefined();
  });

  it('contains a submit button', function (  ) {
    var submitBtn = $("input[type='submit'");
    expect( submitBtn ).toBeDefined();
  });

});