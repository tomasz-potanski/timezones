Timezone: API & management web app
----
Author: Tomasz Potanski, tomasz@potanski.pl

Link: [https://ewan-projekty.com](https://ewan-projekty.com)

Technologies: Express, Angular, JS, Gulp, Scss, Protractor, Mocha, Supertest, AWS, EC2, SSL, EIP, Route 53, Nginx, Supervisord

App allows to compare time in different places in the world. 


Starting an app
---------------

After starting mongo service and creating a colestion with appropriate roles: 

```
node server.js
```

Gulp
----

There is a development gulp task responsible for styles:

```
gulp scss:watch
```


Tests - Mocha
-------------

```
mocha
```

Tests - E2E - protractor
-----

```
webdriver-manager update
```

```
webdriver-manager start
```

```
protractor protractor.conf.js
```