var config = {
    //"url": "mongodb://tzt:123456@ds047955.mongolab.com:47955/timezonesdb", // "emergency" db
    "url": "mongodb://app:app@127.0.0.1:27017/timezonesdb",
    "testing-url": "mongodb://test:test@ds055945.mongolab.com:55945/potan-testing"
};

module.exports = config;