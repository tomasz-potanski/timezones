Endpoints for an app
=====================

What is a token?
----------------

Token is a string that identifies a current session and a user, in particular
it is used to verify if the user has permission to do operation he requested.
It is ofter required to perform CRUD actions in order to make
the interacting between the api and the app more secure.
For most endpoints it needs to be send in the body,
for GET endpoints (if using token is necessary) - it needs to be
given in the url.


Timezones
---------

- GET /api/timezones/:token

Returns a list of all available timezones.
"token" is required in the url.

- POST /api/timezones

Creates a new timezone.
"token" required in the body.

- GET /api/timezones/:timezone_id/:token

Gets a specific timezone.

- PUT /api/timezones/:timezone_id

Changes specific fields of timezone with id: timezone_id.
Token is required in the body.

- DELETE /api/timezones/:timezone_id

Removes this specific timezone.
Token is required in the body.

Users
-----

- POST /api/users/login-check

Endpoint used for logins. Check if credentials are
alright. Returns an access token that is being used
throughout the process of using the app.

- POST /api/users

Creates a new user.

- GET /api/users/fetch/:token

Returns a list of all users.

- GET /api/users/:user_id

Returns a specific user.

- PUT /api/users/:user_id

Changes parameters of this user.

- DELETE /api/users/:user_id

Removes this user.

Endpoints for admins
====================

For these endpoints token is not required, but they are not necessary for the
app to function and can be switched off (commented) in ./server.js. They are helpful
in changing the data using tools such as curl or postman (or any other similar).

Timezones
---------

- POST /api/tz

Creates a new timezone

- GET /api/tz

Returns a list of all timezones.

- GET /api/tz/:timezone_id

Returns this on timezone (with a give id).

- PUT /api/tz/:timezone_id

Changes field in a timezone.

- DELETE /api/tz/:timezone_id

Removes a timezone.

Users
-----

- GET /api/us

Returns a list of all users.

- POST /api/us

Creates a new user.

- GET /api/us/:user_id

Returns one record - user with a given id

- PUT /api/us/:user_id

Changes this user

- DELETE /api/us/:user_id

Removes user with a given id.
