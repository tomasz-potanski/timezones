var express        = require('express');
var router = express.Router();

var User = require('../models/users');
var activeSessions = require( '../utils/active-sessions.js' );


//LOGIN CHECK
router.route('/users/login-check' ).post(function ( req, res ) {
  User.findOne( {'login': req.body.login }, function ( err, doc ) {
    if ( err ) {
      res.send( err );
    }

    if ( doc ) {
      if ( doc.passHash !== req.body.passHash ) {
        res.json({
          status: "error",
          message: "Credentials don't match!"
        });
        return;
      } else {
        var accessHash = activeSessions.generateHash( doc.id );
        activeSessions.addAccess( accessHash, doc.id );

        var name = doc.login;
        if ( doc.firstName ) {
          name = doc.firstName;
        }

        res.json({
          message: 'Credentials are correct!',
          status: "OK",
          accessHash: accessHash,
          firstName: name,
          accessLevel: doc.accessLevel
        });
        return;
      }
    } else {
      res.json({
        status: "error",
        message: "Login doesn't exist!"
      });
      return;
    }

  })
});

//CREAT A NEW User
router.route('/users')

  .post(function(req, res) {
    var user = new User();

    var requiredFields = [
      req.body.login,
      req.body.firstName,
      req.body.lastName,
      req.body.passHash
    ];

    for ( var i = 0; i < requiredFields.length; i++ ) {
      var current = requiredFields[i];
      if ( !current || current == "" ) {
        res.send( err );
        return;
      }
    }

    user.login = req.body.login;
    user.firstName = req.body.firstName;
    user.lastName = req.body.lastName;
    user.passHash = req.body.passHash;

    if ( req.body.accessLevel ) {
      user.accessLevel = req.body.accessLevel;
    } else {
      user.accessLevel = "regular";
    }

    User.findOne( {'login': user.login }, function ( loginCanBeTaken, doc ) {
      if ( doc || loginCanBeTaken != null ) {
        res.json({
          message: 'Login already taken',
          status: "error"
        });
        return;
      } else {
        user.save(function ( err ) {
          if ( err ) {
            res.send( err );
          } else {

            var accessHash = activeSessions.generateHash( user.id );
            activeSessions.addAccess( accessHash, user.id );

            res.json({
              message: 'User created!',
              status: "OK",
              user: user,
              accessHash: accessHash,
              accessLevel: user.accessLevel
            });
            return;
          }
        });
      }
    });


  });

// GET A LIST OF Users
router.route('/users/fetch/:token' ).get(function(req, res) {
  User.find(function(err, users) {
    if (err) {
      res.send( err );
      return;
    } else {

      if ( activeSessions.isActive( req.params.token ) ) {
        activeSessions.updateMoment( req.params.token );

        var isHashRegistered = activeSessions.isHashRegistered( req.params.token );
        if ( isHashRegistered ) {
          var id = activeSessions.idFromHash( req.params.token );
          User.findOne( {'_id': id }, function ( err, doc ) {
            if ( err ) {
              res.json({
                "status": "error",
                "message": "Not authorised. Token not in DB."
              });
              return;
            } else {
                var accessLevel = doc.accessLevel;
                if ( accessLevel === "admin" || accessLevel === "manager" ) {

                  res.json({
                    "status": "OK",
                    "message": "OK, authorised.",
                    "data": users
                  });
                  return;

                } else {
                  res.json({
                    "status": "error",
                    "message": "Not authorised. Not an admin."
                  });
                  return;
                }

            }
          });

        } else {
          res.json({
            "status": "error",
            "message": "Not authorised. Token doesn't exist."
          });
          return;
        }

      } else {
        res.json({
          "status": "error",
          "message": "Not authorised."
        });
        return;
      }

    }
  });
});



// GET SPECIFIC USER
router.route('/users/:user_id')

  .get(function(req, res) {
    User.findById(req.params.user_id, function(err, user) {
      if (err) {
        res.send( err );
        return;
      } else {
        res.json( user );
        return;
      }
    });
  })
  .put(function(req, res) {

    User.findById(req.params.user_id, function(err, user) {

      if (err) {
        res.send( err );
        return;
      } else {

        var fieldsToUpdate = [
          [req.body.login, "login"],
          [req.body.firstName, "firstName"],
          [req.body.lastName, "lastName"],
          [req.body.passHash, "passHash"],
          [req.body.accessLevel, "accessLevel"]
        ];

        for ( var i = 0; i < fieldsToUpdate.length; i++ ) {
          var current = fieldsToUpdate[i];
          if ( current[0] ) {
            user[current[1]] = current[0];
          }
        }

        user.save( function ( err ) {
          if ( err ) {
            res.send( err );
            return;
          } else {
            res.json({
              status: "OK",
              message: 'User updated!',
              user: user
            });
            return;
          }
        });
      }

    });
  })

  .delete(function(req, res) {

    if ( activeSessions.isActive( req.body.token ) ) {
      activeSessions.updateMoment( req.body.token );

      var isHashRegistered = activeSessions.isHashRegistered( req.body.token );
      if ( isHashRegistered ) {
        var loggedUserId = activeSessions.idFromHash( req.body.token );

        User.findOne( {'_id': loggedUserId }, function ( err, doc ) {
          if ( err ) {
            res.json({
              "status": "error",
              "message": "Not authorised. Token not in DB."
            });
            return;
          } else {
            if ( doc ) {
              if ( doc.accessLevel === "admin" || doc.accessLevel === "manager" ) {

                User.remove( {
                  _id: req.params.user_id
                }, function ( err, user ) {
                  if ( err ) {
                    res.send( err );
                    return;
                  } else {
                    res.json( {
                      message: 'Successfully deleted',
                      status: "OK"
                    } );
                    return;
                  }
                });

              } else {
                res.json( {
                  "status": "error",
                  "message": "Not authorised. Not an admin."
                } );
                return;
              } // \if ( doc.accessLevel === "admin" ) {

            } else {
              res.json( {
                "status": "error",
                "message": "Not authorised. Logged user doesn't exist in DB."
              });
              return;
            } // \if ( doc )
          }
        });

      } else {
        res.json({
          "status": "error",
          "message": "Not authorised. Token doesn't exist."
        });
        return;
      } // \if ( isHashRegistered )

    } else {
      res.json({
        "status": "error",
        "message": "Not authorised."
      });
      return;
    } // \if ( activeSessions.isActive( req.body.token ) ) {

  });




module.exports = router;
