var express        = require('express');
var router = express.Router();

var Timezone = require('../models/timezones');
var User = require('../models/users');
var activeSessions = require( '../utils/active-sessions.js' );
var timezoneUtils = require( '../utils/timezone.js' );

router.route('/tz')

  .post(function(req, res) {
    var timezone = new Timezone();

    var requiredFields = [
      req.body.name,
      req.body.nameOfACity,
      req.body.diffToGMT,
      req.body.userLogin
    ];

    for ( var i = 0; i < requiredFields.length; i++ ) {
      var current = requiredFields[i];
      if ( !current ) {
        res.send( err );
        return;
      }
    }

    timezone.name = req.body.name;
    timezone.nameOfACity = req.body.nameOfACity;
    timezone.diffToGMT = req.body.diffToGMT;
    timezone.userLogin = req.body.userLogin;

    timezone.save( function ( err ) {
      if ( err ) {
        res.send( err );
      } else {

        res.json({
          message: 'timezone created!',
          status: "OK"
        });
      }
    });

  });

router.route('/tz' ).get(function(req, res) {
  Timezone.find(function(err, timezones) {
    if (err) {
      res.send( err );
      return;
    } else {
      res.json({
        "status": "OK",
        "data": timezones
      });
      return;
    }
  });
});

// GET SPECIFIC TIMEZONE
router.route('/tz/:timezone_id')

  // get the bear with that id (accessed at GET http://localhost:8080/api/bears/:bear_id)
  .get(function(req, res) {
    Timezone.findById(req.params.timezone_id, function(err, timezone) {
      if ( err ) {
        res.send( err );
        return;
      } else {

          res.json( timezone );
          return;

      }
    });
  });

router.route('/tz/:timezone_id')
  .put(function(req, res) {

    // use our bear model to find the bear we want
    Timezone.findById(req.params.timezone_id, function(err, timezone) {

      if ( err ) {
        res.send( err );
        return;
      } else {

        var fieldsToUpdate = [
          [req.body.name, "name"],
          [req.body.nameOfACity, "nameOfACity"],
          [req.body.diffToGMT, "diffToGMT"],
          [req.body.userLogin, "userLogin"]
        ];

        for ( var i = 0; i < fieldsToUpdate.length; i++ ) {
          var current = fieldsToUpdate[i];
          if ( current[0] ) {
            timezone[current[1]] = current[0];
          }
        }

        timezone.save( function ( err ) {
          if ( err ) {
            res.send( err );
            return;
          } else {
            res.json({
              message: 'Timezone updated!',
              timezone: timezone,
              status: "OK"
            });
            return;
          }
        });

      }
    });
  });

router.route('/tz/:timezone_id' )
  .delete(function(req, res) {
    Timezone.remove({
      _id: req.params.timezone_id
    }, function(err, timezone) {
      if (err) {
        res.send( err );
        return;
      } else {
        res.json({
          message: 'Successfully deleted',
          status: "OK"
        });
        return;
      }
    });
  });


module.exports = router;
