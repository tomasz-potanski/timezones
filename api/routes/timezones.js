var express        = require('express');
var router = express.Router();

var Timezone = require('../models/timezones');
var User = require('../models/users');
var activeSessions = require( '../utils/active-sessions.js' );
var timezoneUtils = require( '../utils/timezone.js' );


//CREAT A NEW TIMEZONE
router.route('/timezones')

  .post(function(req, res) {
    var timezone = new Timezone();

    var requiredFields = [
      req.body.name,
      req.body.nameOfACity,
      req.body.diffToGMT,
      req.body.userLogin,
      req.body.token
    ];

      for ( var i = 0; i < requiredFields.length; i++ ) {
        var current = requiredFields[i];
        if ( !current ) {
          res.json({
            "status": "error",
            "message": "One of the required fields (" + i + ") is lacking."
          });
          return;
        }
      }

    if ( activeSessions.isActive( req.body.token ) ) {
      activeSessions.updateMoment( req.body.token );

      timezone.name = req.body.name;
      timezone.nameOfACity = req.body.nameOfACity;
      timezone.diffToGMT = req.body.diffToGMT;
      timezone.userLogin = req.body.userLogin;

      timezone.save( function ( err ) {
        if ( err ) {
          res.send( err );
        } else {

          res.json({
            message: 'Timezone created!',
            status: "OK",
            timezone: timezone
          });
        }
      });
    } else {
      res.json({
        "status": "error",
        "message": "Not authorised."
      });
    }

  });

// GET A LIST OF TIMEZONES
router.route('/timezones/:token' ).get(function(req, res) {
  Timezone.find(function(err, timezones) {
    if (err) {
      res.send( err );
      return;
    } else {

      if ( activeSessions.isActive( req.params.token ) ) {
        activeSessions.updateMoment( req.params.token );

        var isHashRegistered = activeSessions.isHashRegistered( req.params.token );
        if ( isHashRegistered ) {
          var id = activeSessions.idFromHash( req.params.token );
          User.findOne( {'_id': id }, function ( err, doc ) {
            if ( err ) {
              res.json({
                "status": "error",
                "message": "Not authorised. Token not in DB."
              });
              return;
            } else {
              var login = doc.login;
              var accessLevel = doc.accessLevel;
              var resultTimezones = timezoneUtils.filterTimezonesUsingLoginAndPermissions(
                timezones,
                login,
                accessLevel
              );

              res.json({
                "status": "OK",
                "data": resultTimezones
              });
              return;
            }
          });

        } else {
          res.json({
            "status": "error",
            "message": "Not authorised. Token doesn't exist."
          });
          return;
        }
      } else {
        res.json({
          "status": "error",
          "message": "Not authorised."
        });
        return;
      }
    }

  });
});

// GET SPECYFIC TIMEZONE
router.route('/timezones/:timezone_id/:token')

  .get(function(req, res) {
    Timezone.findById(req.params.timezone_id, function(err, timezone) {
      if ( err ) {
        res.send( err );
        return;
      } else {
        if ( activeSessions.isActive( req.params.token ) ) {
          activeSessions.updateMoment( req.params.token );
          res.json({
            "status": "OK",
            "message": "OK, authorised.",
            "timezone": timezone
          });
          return;
        } else {
          res.json({
            "status": "error",
            "message": "Not authorised."
          });
          return;
        }
      }
    });
  });

router.route('/timezones/:timezone_id')
  .put(function(req, res) {

    Timezone.findById(req.params.timezone_id, function(err, timezone) {

      if ( err ) {
        res.send( err );
        return;
      } else {

        var requiredFields = [
          req.body.token
        ];

        for ( var i = 0; i < requiredFields.length; i++ ) {
          var current = requiredFields[i];
          if ( !current ) {
            res.send( err );
            return;
          }
        }

        var fieldsToUpdate = [
          [req.body.name, "name"],
          [req.body.nameOfACity, "nameOfACity"],
          [req.body.diffToGMT, "diffToGMT"],
          [req.body.userLogin, "userLogin"]
        ];

        for ( var i = 0; i < fieldsToUpdate.length; i++ ) {
          var current = fieldsToUpdate[i];
          if ( current[0] ) {
            timezone[current[1]] = current[0];
          }
        }

        if ( activeSessions.isActive( req.body.token ) ) {
          activeSessions.updateMoment( req.body.token );
          timezone.save( function ( err ) {
            if ( err ) {
              res.send( err );
              return;
            } else {
              res.json({
                message: 'Timezone updated!',
                timezone: timezone,
                status: "OK"
              });
              return;
            }
          } );
        } else {
          res.json({
            "status": "error",
            "message": "Not authorised."
          });
          return;
        }
      }
    });
  })

  .delete(function(req, res) {
    Timezone.remove({
      _id: req.params.timezone_id
    }, function(err, timezone) {
      if (err) {
        res.send( err );
        return;
      } else {
        if ( activeSessions.isActive( req.body.token ) ) {
          activeSessions.updateMoment( req.body.token );
          res.json( {
            message: 'Successfully deleted',
            status: "OK"
          });
          return;
        } else {
          res.json({
            "status": "error",
            "message": "Not authorised."
          });
          return;
        }
      }
    });
  });



module.exports = router;