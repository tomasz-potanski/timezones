var express        = require('express');
var router = express.Router();

var User = require('../models/users');
var activeSessions = require( '../utils/active-sessions.js' );

// GET A LIST OF Users
router.route('/us' ).get(function(req, res) {
  User.find(function(err, users) {
    if (err) {
      res.send( err );
      return;
    } else {
      res.json( users );
      return;
    }
  });
});

router.route('/us')

  .post(function(req, res) {
    var user = new User();

    var requiredFields = [
      req.body.login,
      req.body.firstName,
      req.body.lastName,
      req.body.passHash
    ];

    for ( var i = 0; i < requiredFields.length; i++ ) {
      var current = requiredFields[i];
      if ( !current ) {
        res.send( err );
        return;
      }
    }

    user.login = req.body.login;
    user.firstName = req.body.firstName;
    user.lastName = req.body.lastName;
    user.passHash = req.body.passHash;

    if ( req.body.accessLevel ) {
      user.accessLevel = req.body.accessLevel;
    } else {
      user.accessLevel = "regular";
    }

    User.findOne( {'login': user.login }, function ( loginCanBeTaken, doc ) {
      if ( doc || loginCanBeTaken != null ) {
        res.json({
          message: 'Login already taken',
          status: "error"
        });
        return;
      } else {
        user.save(function ( err ) {
          if ( err ) {
            res.send( err );
          } else {

            var accessHash = activeSessions.generateHash( user.id );
            activeSessions.addAccess( accessHash, user.id );

            res.json({
              message: 'User created!',
              status: "OK",
              user: user,
              accessHash: accessHash,
              accessLevel: user.accessLevel
            });
            return;
          }
        });
      }
    });


  });

// GET SPECYFIC USER
router.route('/us/:user_id')

  .get(function(req, res) {
    User.findById(req.params.user_id, function(err, user) {
      if (err) {
        res.send( err );
        return;
      } else {
        res.json( user );
        return;
      }
    });
  });

router.route('/us/:user_id')
.put(function(req, res) {

  User.findById(req.params.user_id, function(err, user) {

    if (err) {
      res.send( err );
      return;
    } else {

      var fieldsToUpdate = [
        [req.body.login, "login"],
        [req.body.firstName, "firstName"],
        [req.body.lastName, "lastName"],
        [req.body.passHash, "passHash"],
        [req.body.accessLevel, "accessLevel"]
      ];

      for ( var i = 0; i < fieldsToUpdate.length; i++ ) {
        var current = fieldsToUpdate[i];
        if ( current[0] ) {
          user[current[1]] = current[0];
        }
      }

      user.save( function ( err ) {
        if ( err ) {
          res.send( err );
          return;
        } else {
          res.json({
            status: "OK",
            message: 'User updated!',
            user: user
          });
          return;
        }
      });
    }

  });
});

router.route('/us/:user_id')
  .delete(function(req, res) {
    User.remove({
      _id: req.params.user_id
    }, function(err, user) {
      if (err) {
        res.send( err );
        return;
      } else {
        res.json({
          message: 'Successfully deleted',
          status: "OK"
        });
        return;
      }
    });
  });

module.exports = router;