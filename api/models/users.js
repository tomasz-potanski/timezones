var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;


var UserSchema   = new Schema({
  login: String,
  firstName: String,
  lastName: String,
  passHash: String,
  accessLevel: String // "regular", "manager", "admin"
});


module.exports = mongoose.model('User', UserSchema);
