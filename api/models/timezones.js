var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;


var TimezoneSchema   = new Schema({
  name: String,
  nameOfACity: String,
  diffToGMT: String,
  userLogin: String
});


module.exports = mongoose.model('Timezone', TimezoneSchema);