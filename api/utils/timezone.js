var timezoneUtils = {

  // Users can see (and CRUD) only timezones they have created.
  // Admin can CRUD on all timezones.
  filterTimezonesUsingLoginAndPermissions: function ( timezones, login, accessLevel ) {
      if ( accessLevel === "admin" ) {
        return timezones;
      } else {
        var result = [];

        for ( var i = 0; i < timezones.length; i++ ) {
          var current = timezones[i];

          if ( current.userLogin === login ) {
            result.push( current );
          }
        }

        return result;
      }
  }
};

module.exports = timezoneUtils;
