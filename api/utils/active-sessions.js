var moment = require('moment');
var forge = require('node-forge');

var User = require('../models/users');

var sessionDuration = 10; // [min]


var activeSessions = {
    //map: hash -> moment of last access
  sessions: {},

  hashToUserId: {},

  isActive: function ( hash ) {
    if ( this.sessions.hasOwnProperty( hash ) ) {
      var thisMoment = this.sessions[hash];

      return moment().isBefore( thisMoment.add( sessionDuration, 'minutes' ) );

    } else {
      return false;
    }
  },

  addAccess: function ( hash, id ) {
    this.sessions[hash] = moment();
    if ( !this.hashToUserId.hasOwnProperty( hash ) && hash !== "undefined" ) {
      this.hashToUserId[hash] = id;
    } else {
      //duplication
    }
  },

  // alias
  updateMoment: function ( hash ) {
      this.addAccess( hash );
  },

  // convert user_id to hash
  generateHash: function ( id ) {
    var md = forge.md.sha256.create();
    md.update( id );

    return md.digest().toHex();
  },

  isHashRegistered: function ( hash ) {
      if ( !this.hashToUserId.hasOwnProperty( hash ) ) {
        return false;
      }

    return true;

  },

  idFromHash: function ( hash ) {
      if ( this.isHashRegistered( hash ) ){
        return this.hashToUserId[hash];
      }
  }
};

module.exports = activeSessions;
