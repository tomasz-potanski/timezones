angular.module('SignupCtrl', []).controller('SignupController',
  ['$scope', '$http', '$location', 'LoginFactory', 'ValidationFactory',
    function( $scope, $http, $location, LoginFactory, ValidationFactory ) {

        var self = this;
        this.invalidCredentials = false;
        this.requestLoading = false;

        if ( LoginFactory.isLoggedIn() ) {
            $location.url('/panel');
        }

        this.validateCredentials = function () {
            var validators = [
              [ValidationFactory.textNotEmpty, self.login],
              [ValidationFactory.textNotEmpty, self.firstName],
              [ValidationFactory.textNotEmpty, self.lastName],
              [ValidationFactory.textNotEmpty, self.pass],
              [ValidationFactory.textNotEmpty, self.pass2],
              [ValidationFactory.textsIdentical, self.pass, self.pass2]
            ];

            for ( var i = 0; i < validators.length; i++ ) {
                var current = validators[i];
                var func = current[0];
                var arg1 = current[1];

                var thisOutcome;

                if ( current.length === 3 ) {
                    //verification if passwords match
                    var arg2 = current[2];

                    thisOutcome = func( arg1, arg2 );
                } else {
                    thisOutcome = func( arg1 );
                }

                if ( !thisOutcome ) {
                    return false;
                }
            }

            return true;
        };

      /**
       * Submitting sign-up form
       */
        this.submit = function ( $event ) {
            $event.preventDefault();

            if ( self.validateCredentials() ) {
                var url = '/api/users';
                var salt = "EURO3";

                var md = forge.md.sha256.create();
                md.update(self.pass + salt);
                var hash = md.digest().toHex();

                var data = JSON.stringify({
                    "login": this.login,
                    "firstName": this.firstName,
                    "lastName": this.lastName,
                    "passHash": hash
                });

                self.requestLoading = true;
                $http({
                    method: 'POST',
                    url: url,
                    data: data,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then( function ( data ) {
                    if ( data && data.data && data.data.status && data.data.status === "OK" ){
                        self.requestLoading = false;
                        LoginFactory.login(
                          data.data.accessHash,
                          self.login,
                          self.firstName,
                          data.data.accessLevel
                        );
                    } else {
                        self.requestLoading = false;
                        self.invalidCredentials = true;
                    }
                }, function () {
                    self.requestLoading = false;
                    self.invalidCredentials = true;
                });
            } else {
                self.requestLoading = false;
                self.invalidCredentials = true;
            }

        };

    }
  ]
);