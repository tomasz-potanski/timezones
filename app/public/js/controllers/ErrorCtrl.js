angular.module('ErrorCtrl', []).controller('ErrorController',
  [ 'LoginFactory',
    function( LoginFactory ) {

    this.destroy = function ( $event ) {
        $event.preventDefault();

        LoginFactory.logout();
    };


    }]);
