angular.module('LoginCtrl', []).controller('LoginController',
  ['$scope', '$http', '$cookies', 'LoginFactory',
    function( $scope, $http, $cookies, LoginFactory ) {

        var url = '/api/users/login-check';
        var salt = "EURO3";
        var self = this;
        this.incorrectCredentials = false;
        var confirmedFromCookies = $cookies.get( 'cookieConfirmed' );
        this.requestLoading = false;

        if ( LoginFactory.isLoggedIn() ) {
            LoginFactory.redirect( '/panel' );
        }
        else if ( confirmedFromCookies && confirmedFromCookies === "true" ) {
            this.cookieConfirmed = true;
        } else {
            this.cookieConfirmed = false;
        }

      /**
       * Executed after user agrees to use cookie
       */
        this.confirmCookie = function ( $event ) {
            $event.preventDefault();

            $cookies.put( 'cookieConfirmed', 'true' );
            self.cookieConfirmed = true;
        };

      /**
       * Submitting login form
       */
        this.submit = function( $event ) {
            $event.preventDefault();

            var md = forge.md.sha256.create();
            md.update(this.pass + salt);
            var hash = md.digest().toHex();

            var data = JSON.stringify({
                "login": this.login,
                "passHash": hash
            });

            self.requestLoading = true;
            $http({
                method: 'POST',
                url: url,
                data: data,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function ( data ) {
                if ( data && data.data && data.data.status && data.data.status === "OK" ){
                    self.incorrectCredentials = false;
                    self.requestLoading = false;

                    LoginFactory.login(
                      data.data.accessHash,
                      self.login,
                      data.data.firstName,
                      data.data.accessLevel
                    );

                } else {
                    self.requestLoading = false;
                    self.incorrectCredentials = true;
                }
            }, function (  ) {
                self.requestLoading = false;
                self.incorrectCredentials = true;
            } );
        };


    }]);