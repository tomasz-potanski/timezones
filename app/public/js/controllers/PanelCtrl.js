angular.module('PanelCtrl', []).controller('PanelController',
  ['$scope', '$http', '$timeout', '$cookies', 'LoginFactory', 'ValidationFactory', '$interval',
    function( $scope, $http, $timeout, $cookies, LoginFactory, ValidationFactory, $interval ) {

        var url = '/api/timezones'; // for timezone related api
        var self = this;
        this.timezones = [];
        this.timeOffset = (new Date).getTimezoneOffset() / 60;
        this.retriesSoFar = 0;
        this.retriesSoFarUsers = 0;

        this.accessToken = $cookies.get( 'accessToken' );
        this.login = $cookies.get( 'login' );
        this.firstName = $cookies.get( 'firstName' );
        this.isUserAManager = $cookies.get( 'accessLevel' ) === "manager" || $cookies.get( 'accessLevel' ) === "admin" || false;
        this.loading = true; // for big spinner
        this.userIsAdmin = $cookies.get( 'accessLevel' ) === "admin" || false;

        this.newTimezoneDialogVisible = false;
        this.invalidNewTimezoneData = false;

        this.editTimezoneDialogVisible = false;
        this.invalidEditTimezoneData = false;

        this.newUserDialogVisible = false;
        this.invalidNewUserData = false;

        this.editUserDialogVisible = false;
        this.invalidEditUserData = false;

        this.userEditMode = false;

        this.lastTimeout = null;
        this.editModeUsers = false;

        this.requestLoading = false;

        var accessLevelsAll = [
            {name: "regular", id: 0},
            {name: "manager", id: 1},
            {name: "admin", id: 2}
        ];
        if ( this.userIsAdmin ) {
            this.accessLevels = accessLevelsAll;
        } else {
            this.accessLevels = [];
            this.accessLevels.push( accessLevelsAll[0] );
            this.accessLevels.push( accessLevelsAll[1] );
        }

        this.accessLevel = this.accessLevels[0];
        this.accessLevelEdit = this.accessLevels[0];

        this.countOfActiveRequests = 0; //for small spinner

        // If at least one request is being processed
        // a small spinner in the top-left corner is presented to
        // the user
        this.increaceActiveRequestCount = function () {
            self.countOfActiveRequests++;
        };

        this.decreaseActiveRequestCount = function () {
            self.countOfActiveRequests--;
        };

        this.validateAndEditAUser = function ( $event ) {
            $event.preventDefault();

            var specificUrl = '/api/users' + "/" + self.itemId;
            if ( self.validateUserCredentials( "edit" ) ) {
                var salt = "EURO3";

                var md = forge.md.sha256.create();
                md.update(self.editUserPass + salt);
                var hash = md.digest().toHex();

                var dataObj = {
                    "login": self.editUserLogin,
                    "firstName": self.editUserFirstName,
                    "lastName": self.editUserLastName,
                    "token": self.accessToken,
                    "accessLevel": self.accessLevelEdit.name
                };

                if ( self.editUserPass !== "" ) {
                    dataObj["passHash"] = hash;
                }

                var data = JSON.stringify( dataObj );

                self.increaceActiveRequestCount();
                $http({
                    method: 'PUT',
                    url: specificUrl,
                    data: data,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(
                 function ( response ) {
                     self.decreaseActiveRequestCount();
                     if ( response && response.data && response.data.status && response.data.status === "OK" ) {
                         self.fetchUsers();
                         self.editUserDialogVisible = false;
                     } else {
                         self.invalidEditUserData = true;
                     }
                 },
                  function () {
                      // retry?
                      self.decreaseActiveRequestCount();
                      self.invalidEditUserData = true;
                  }
                );

            } else {
                self.invalidEditUserData = true;
            }
        };

        this.editUserDialogOn = function ( user ) {
            self.editUserLogin      = user.login;
            self.editUserFirstName  = user.firstName;
            self.editUserLastName   = user.lastName;
            self.editUserPass       = "";
            self.editUserPass2      = "";
            self.itemId             = user._id;

            self.accessLevelEdit    = self.accessLevels[0];
            for ( var i = 0; i < self.accessLevels.length; i++ ) {
                var current = self.accessLevels[i];
                if ( current.name === user.accessLevel ) {
                    self.accessLevelEdit = current;
                    break;
                }
            }

            self.newTimezoneDialogVisible = false;
            self.editTimezoneDialogVisible = false;
            self.newUserDialogVisible = false;
            self.editUserDialogVisible = true;
        };

        this.hideEditUserDialog = function ( $event ) {
            $event.preventDefault();

            self.editUserDialogVisible = false;
        };

        this.removeUser = function ( user ) {
            var id = user._id;

            if ( id && typeof id === "string" ) {
                var specificUrl = '/api/users' + "/" + id;

                var data = {
                    "token": self.accessToken
                };

                self.increaceActiveRequestCount();
                $http({
                    method: 'DELETE',
                    url: specificUrl,
                    data: data,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function ( response ) {
                    self.decreaseActiveRequestCount();
                    if ( response && response.data && response.data.status && response.data.status === "OK" ) {
                        self.fetchUsers();
                    } else {
                        // error removing the user
                    }
                }, function () {
                    self.decreaseActiveRequestCount();
                    // error removing the user
                })
            }
        };

        this.validateUserCredentials = function ( dataType ) {
            var validators;
            if ( dataType === "edit" ) {
                validators = [
                    [ValidationFactory.textNotEmpty, self.editUserLogin],
                    [ValidationFactory.textNotEmpty, self.editUserFirstName],
                    [ValidationFactory.textNotEmpty, self.editUserLastName],
                    //[ValidationFactory.textNotEmpty, self.editUserPass],
                    //[ValidationFactory.textNotEmpty, self.editUserPass2],
                    [ValidationFactory.textsIdentical, self.editUserPass, self.editUserPass2]
                ];
            } else {
                validators = [
                    [ValidationFactory.textNotEmpty, self.newUserLogin],
                    [ValidationFactory.textNotEmpty, self.newUserFirstName],
                    [ValidationFactory.textNotEmpty, self.newUserLastName],
                    [ValidationFactory.textNotEmpty, self.newUserPass],
                    [ValidationFactory.textNotEmpty, self.newUserPass2],
                    [ValidationFactory.textsIdentical, self.newUserPass, self.newUserPass2]
                ];
            }

            for ( var i = 0; i < validators.length; i++ ) {
                var current = validators[i];
                var func = current[0];
                var arg1 = current[1];

                var thisOutcome;

                if ( current.length === 3 ) {
                    //verification if passwords match
                    var arg2 = current[2];

                    thisOutcome = func( arg1, arg2 );
                } else {
                    thisOutcome = func( arg1 );
                }

                if ( !thisOutcome ) {
                    return false;
                }
            }

            return true;
        };

        this.validateAndCreateAUser = function ( $event ) {
            $event.preventDefault();

            if ( self.validateUserCredentials() ) {
                var url = '/api/users';
                var salt = "EURO3";

                var md = forge.md.sha256.create();
                md.update(self.newUserPass + salt);
                var hash = md.digest().toHex();

                var data = JSON.stringify({
                    "login": self.newUserLogin,
                    "firstName": self.newUserFirstName,
                    "lastName": self.newUserLastName,
                    "passHash": hash,
                    "accessLevel": self.accessLevel.name
                });

                self.increaceActiveRequestCount();
                $http({
                    method: 'POST',
                    url: url,
                    data: data,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(
                  function ( response ) {
                      self.decreaseActiveRequestCount();
                      if ( response && response.data && response.data.status && response.data.status === "OK" ){
                          self.fetchUsers();
                          self.invalidCredentials = false;
                          self.newUserDialogVisible = false;
                      } else {
                          self.invalidCredentials = true;
                      }
                  },
                  function () {
                      self.decreaseActiveRequestCount();
                      self.invalidNewUserData = true;
                  }
                );
            } else {
                self.invalidNewUserData = true;
            }
        };

        this.hideNewUserDialog = function ( $event ) {
            $event.preventDefault();

            self.newUserDialogVisible = false;
        };

        this.showNewUserDialog = function ( $event ) {
            $event.preventDefault();

            self.newTimezoneDialogVisible = false;
            self.editTimezoneDialogVisible = false;
            self.editUserDialogVisible = false;
            self.newUserDialogVisible = true;
        };

        this.editModeUsersOn = function ( $event ) {
            $event.preventDefault();

            self.editModeUsers = true;
        };

        self.editModeUsersOff = function ( $event ) {
            $event.preventDefault();

            self.editModeUsers = false;
        };

        this.editTimezoneDialogOn = function ( item ) {
            self.editTimezoneName = item.name;
            self.editTimezoneDiff = item.diffToGMT;
            self.editTimezoneCity = item.nameOfACity;
            self.itemId           = item._id;

            self.newTimezoneDialogVisible = false;
            self.newUserDialogVisible = false;
            self.editUserDialogVisible = false;
            self.editTimezoneDialogVisible = true;
        };

        this.validateAndUpdateTimezone = function ( $event ) {
            $event.preventDefault();

            var specificUrl = url + "/" + self.itemId;

            if ( self.validateNewTimezone( "edit" ) ) {
                var data = JSON.stringify({
                    "diffToGMT": self.editTimezoneDiff.toString(),
                    "nameOfACity": self.editTimezoneCity,
                    "name": self.editTimezoneName,
                    "token": self.accessToken
                });

                self.increaceActiveRequestCount();
                $http({
                    method: 'PUT',
                    url: specificUrl,
                    data: data,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(
                  function( response ) {
                      self.decreaseActiveRequestCount();
                      if ( response && response.data && response.data.status && response.data.status === "OK" ) {
                        self.fetchTimezones();
                        self.editTimezoneDialogVisible = false;
                      } else {
                          self.invalidEditTimezoneData = false;
                      }
                  }, function () {
                      self.decreaseActiveRequestCount();
                  }
                );

            } else {
                self.invalidEditTimezoneData = true;
            }
        };

        this.hideEditTimezoneDialog = function ( $event ) {
            $event.preventDefault();

            this.editTimezoneDialogVisible = false;
        };

        this.removeTimezone = function ( item ) {
            var id = item._id;

            if ( id && typeof id === "string" ) {
                var specificUrl = url + "/" + id;

                var data = JSON.stringify({
                    "token": self.accessToken
                });

                self.increaceActiveRequestCount();
                $http({
                    method: 'DELETE',
                    url: specificUrl,
                    data: data,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function ( response ) {
                    self.decreaseActiveRequestCount();
                    if ( response && response.data && response.data.status && response.data.status === "OK" ) {
                        self.fetchTimezones();
                    } else {
                        // error removing the timezone
                    }
                }, function () {
                 // error removing the timezone
                    self.decreaseActiveRequestCount();
                })
            }
        };

        this.logout = function ( $event ) {
            $event.preventDefault();

            LoginFactory.logout();
        };

        this.editModeOn = function ( $event ) {
            $event.preventDefault();

            self.editMode = true;
        };

        this.editModeOff = function ( $event ) {
            $event.preventDefault();

            self.editMode = false;
        };

      /**
       * Validates data for /new/ or /current editing/ timezone
       * @param dataType specifies data to validate
       * @returns {boolean}
       */
        this.validateNewTimezone = function( dataType ) {
            var validators;
            if ( dataType === "edit" ){
                validators = [
                    [ValidationFactory.textNotEmpty, self.editTimezoneName],
                    [ValidationFactory.textNotEmpty, self.firstName],
                    [ValidationFactory.textNotEmpty, self.editTimezoneCity],
                    [ValidationFactory.typeofNotUndefined, self.editTimezoneDiff],
                    [ValidationFactory.validTimeDiff, self.editTimezoneDiff]
                ];
            } else {
                validators = [
                    [ValidationFactory.textNotEmpty, self.newTimezoneName],
                    [ValidationFactory.textNotEmpty, self.firstName],
                    [ValidationFactory.textNotEmpty, self.newTimezoneCity],
                    [ValidationFactory.typeofNotUndefined, self.newTimezoneDiff],
                    [ValidationFactory.validTimeDiff, self.newTimezoneDiff]
                ];
            }

            for ( var i = 0; i < validators.length; i++ ) {
                var current = validators[i];
                var func = current[0];
                var arg1 = current[1];

                var thisOutcome = func( arg1 );

                if ( !thisOutcome ) {
                    return false;
                }
            }

            return true;
        };

        this.validateAndCreateTimezone = function ( $event ) {
            $event.preventDefault();

            var specificUrl = url;

            if ( self.validateNewTimezone() ) {
                var data = JSON.stringify({
                    "diffToGMT": self.newTimezoneDiff.toString(),
                    "nameOfACity": self.newTimezoneCity,
                    "name": self.newTimezoneName,
                    "userLogin": self.login,
                    "token": self.accessToken
                });

                self.increaceActiveRequestCount();
                $http({
                    method: 'POST',
                    url: specificUrl,
                    data: data,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function ( response ) {
                    self.decreaseActiveRequestCount();
                    if ( response && response.data && response.data.status && response.data.status === "OK" ){
                        self.fetchTimezones();
                    } else {
                        this.invalidNewTimezoneData = true;
                    }
                }, function () {
                 //error
                    self.decreaseActiveRequestCount();
                    this.invalidNewTimezoneData = true;
                })
            } else {
                this.invalidNewTimezoneData = true;
            }
        };

        this.showNewTimezoneDialog = function ( $event ) {
            $event.preventDefault();

            self.editTimezoneDialogVisible = false;
            self.newUserDialogVisible = false;
            self.editUserDialogVisible = false;
            self.newTimezoneDialogVisible = true;
        };

        this.hideNewTimezoneDialog = function ( $event ) {
            $event.preventDefault();

            self.newTimezoneDialogVisible = false;
        };

        this.fetchUsers = function () {
            var specificUrl = '/api/users' + "/fetch/" + self.accessToken;

            self.increaceActiveRequestCount();
            $http({
                method: 'GET',
                url: specificUrl
            }).then(
                function ( response ) {
                    self.decreaseActiveRequestCount();
                    if ( response && response.data && response.data.status && response.data.status === "OK" ) {
                        self.users = response.data.data;
                    }
                }, function () {
                    // error fetching
                  self.decreaseActiveRequestCount();
                  if ( self.retriesSoFarUsers < 4 ) {
                      self.retriesSoFarUsers++;
                      self.fetchUsers();
                  } else {
                      LoginFactory.logout();
                  }
                }
            );
        };


        this.fetchTimezones = function (  ) {
            var specificUrl = url + "/" + self.accessToken;

            self.increaceActiveRequestCount();
            $http({
                method: 'GET',
                url: specificUrl
            }).then(function ( data ) {
                self.decreaseActiveRequestCount();
                if ( data && data.data && data.data.status && data.data.status === "OK" ) {
                    self.timezones = [];
                    var allTimezones = data.data.data; // ;)
                    var UTCMoment = moment().add( self.timeOffset, 'hours' );

                    for ( var i = 0; i < allTimezones.length; i++ ) {
                        var current = allTimezones[i];
                        if ( current.hasOwnProperty( "userLogin" ) && current.userLogin === self.login ) {
                            current.belongsToCurrentUser = true;
                            current.ownerText = "You are the owner"
                        } else {
                            current.ownerText = "Owner: " + current.userLogin;
                        }
                        current.diffToGMT = parseInt( current.diffToGMT );

                        if ( current.diffToGMT > 0 ) {
                            current.sign = "+" + current.diffToGMT;
                        } else if ( current.diffToGMT < 0 ) {
                            current.sign = current.diffToGMT.toString();
                        } else {
                            current.sign = "";
                        }

                        current.moment = moment( UTCMoment ).add( current.diffToGMT, 'hours' ).format( 'HH:mm:ss' );
                        self.timezones.push( current );

                    }

                    self.loading = false;
                    self.retriesSoFar = 0;
                    self.newTimezoneDialogVisible = false;
                    if ( self.lastTimeout ) {
                        $interval.cancel( self.lastTimeout );
                    }
                    self.lastTimeout = $interval( self.updateTime, 1000 );

                } else {
                    LoginFactory.logout();
                }

            }, function () {
                //error fetching
                self.decreaseActiveRequestCount();
                if ( self.retriesSoFar < 4 ) {
                    self.retriesSoFar++;
                    self.fetchTimezones();
                } else {
                    LoginFactory.redirect( '/login' );
                }
            });
        };

        this.updateTime = function () {
            var UTCMoment = moment().add( self.timeOffset, 'hours' );

            for ( var i = 0; i < self.timezones.length; i++ ) {
                var current = self.timezones[i];

                current.moment = moment( UTCMoment ).add( current.diffToGMT, 'hours' ).format('HH:mm:ss');
            }
        };

        if ( !LoginFactory.isLoggedIn() ) {
            LoginFactory.redirect( '/login' );
        } else {
            this.fetchTimezones();
            if ( self.isUserAManager ) {
                self.fetchUsers();
            }
        }

    }
  ]
);