angular.module( 'timezoneApp' )
  .factory( 'LoginFactory',
    ['$cookies', '$location',
      function LoginFactory( $cookies, $location ) {
        'use strict';

        var exports = {};
        var redirectsSoFar = 0;
        var redirectsLimit = 30;

        exports.isLoggedIn = function () {
          var accessToken = $cookies.get( 'accessToken' );
          var login = $cookies.get( 'login' );
          var firstName = $cookies.get( 'firstName' );

          if ( accessToken == null ) {
            return false;
          }
          else if ( login == null ) {
            return false;
          }
          else if ( firstName == null ) {
            return false;
          }

          return true;
        };

        exports.redirect = function( url ) {
          if ( redirectsSoFar < redirectsLimit ) {
            redirectsSoFar++;
            $location.url( url );
          }
        };

        exports.login = function ( accessHash, login, firstName, accessLevel ) {

          $cookies.put( 'accessToken', accessHash );
          $cookies.put( 'login', login );
          $cookies.put( 'firstName', firstName );
          $cookies.put( 'accessLevel', accessLevel );

          $location.url('/panel');
        };

        exports.logout = function () {
          $cookies.remove( 'accessToken' );
          $cookies.remove( 'login' );
          $cookies.remove( 'firstName' );
          $cookies.remove( 'accessLevel' );

          $location.url('/login');
        };

        return exports;
      }
    ]
  );
