angular.module( 'timezoneApp' )
  .factory( 'ValidationFactory',
    [
      function ValidationFactory( ) {
        'use strict';

        var exports = {};

        exports.textNotEmpty = function ( s ) {
            if ( s && typeof s === "string" && s.length > 0 ) {
              return true;
            }

          return false;
        };

        exports.typeofNotUndefined = function ( value ) {
            if ( typeof value === "undefined" ) {
              return false;
            }

          return true;
        };

        exports.validTimeDiff = function ( n ) {
            if ( typeof n === "number" ) {
              if ( n >= -12 && n <= 12 ) {
                return true;
              }
            }

          return false;
        };

        exports.textsIdentical = function ( s1, s2 ) {
          if ( s1 && s2 ) {
            if ( typeof s1 === "string" && typeof s2 === "string" ) {
              if ( s1.length === s2.length ) {
                for ( var i = 0; i < s1.length; i++ ) {
                  var c1 = s1[i];
                  var c2 = s2[i];
                  if ( c1 !== c2 ) {
                    return false;
                  }

                }

                return true;
              }
            }
          } else {
            if ( !s1 && !s2 ) {
              return true;
            }
          }

          return false;
        };

        return exports;
      }
    ]
  );
