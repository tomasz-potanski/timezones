angular.module('timezoneApp',
  [
    //libs:
    'ngRoute', 'ngCookies',

    //controllers:
    'LoginCtrl', 'PanelCtrl', 'SignupCtrl', 'ErrorCtrl',

    'appRoutes'
  ]
);

