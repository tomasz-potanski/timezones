angular.module('appRoutes', []).config(
  ['$routeProvider',
    function( $routeProvider ) {

      $routeProvider

        .when('/login', {
          templateUrl: 'views/login.html',
          controller: 'LoginController',
          controllerAs: 'loginCtrl'
        })
        .when('/signup', {
          templateUrl: 'views/signup.html',
          controller: 'SignupController',
          controllerAs: 'signupCtrl'
        })
        .when('/panel', {
          templateUrl: 'views/panel.html',
          controller: 'PanelController',
          controllerAs: 'panelCtrl'
        })
        .when('/error', {
          templateUrl: 'views/error.html',
          controller: 'ErrorController',
          controllerAs: 'errorCtrl'
        })
        .otherwise({
          redirectTo: '/login'
        });

    }
  ]
);