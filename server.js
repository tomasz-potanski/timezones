// server.js

// modules =================================================
var express        = require('express');
var app            = express();
var bodyParser     = require('body-parser');
var methodOverride = require('method-override');
var mongoose        = require('mongoose');

// routes
var routerTimezones         = require('./api/routes/timezones');
var routerUsers         = require('./api/routes/users');
var routerUsersAdmin         = require('./api/routes/users_admin');
var routerTimezonesAdmin         = require('./api/routes/timezones_admin');

// configuration ===========================================

// config files
var db = require('./api/config/db');

var port = process.env.PORT || 8082;

// normal configuration
mongoose.connect(db.url);
// testing configuration
//mongoose.connect(db["testing-url"]);

app.use(bodyParser());

// get all data/stuff of the body (POST) parameters
// parse application/json
app.use(bodyParser.json());

// parse application/vnd.api+json as json
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
app.use(methodOverride('X-HTTP-Method-Override'));

app.use(express.static(__dirname + '/app/public'));

// routes ==================================================
app.use( '/api', routerTimezones );
app.use( '/api', routerUsers );
app.use( '/api', routerUsersAdmin );
app.use( '/api', routerTimezonesAdmin );

app.listen(port);

console.log('Magic happens on port ' + port);

exports = module.exports = app;
