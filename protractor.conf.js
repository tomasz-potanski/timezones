exports.config = {

  allScriptsTimeout: 15000,

  specs: [
    'e2e-tests/*.js'
  ],

  //multiCapabilities: [{
  //  'browserName': 'chrome'
  //}, {
  //  'browserName': 'phantomjs'
  //}],
  capabilities: {
    browserName: 'chrome'
  },

  chromeOnly: true,

  baseUrl: 'http://localhost:8082/',

  seleniumAddress: 'http://localhost:4444/wd/hub',

  framework: 'jasmine',

  jasmineNodeOpts: {
    defaultTimeoutInterval: 30000
  }

};
