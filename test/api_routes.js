var should = require('should');
var assert = require('assert');
var request = require('supertest');
var mongoose = require('mongoose');

var dbConfig = require('../api/config/db.js');

request = request('http://localhost:8082/api');


describe('Routing', function() {
  var mongoUrl = dbConfig["testing-url"];
  var accessHash;
  var userId;
  var userLogin;
  var timezoneId;

  before(function ( done ) {
    mongoose.connect(mongoUrl);
    done();
  });

  describe('Sign up', function () {

    it( 'user should be able to create an account', function ( done ) {
      userLogin = "myLoginTesting" + Math.floor( (Math.random() * 1000) + 1 );

      request.post( '/users' ).send( JSON.stringify( {
          "login": userLogin,
          "firstName": "aaaaaa",
          "lastName": "bbbbbb",
          "passHash": "superSecretPass!"
        } ) )
        .set( 'Content-Type', 'application/json' )
        .set( 'Accept', 'application/json' )
        .expect( 200 )
        .end( function ( err, res ) {
          if ( err ) {
            return done( err );
          }

          res.body.should.have.property( 'status' );
          res.body.status.should.equal( 'OK' );
          res.body.should.have.property( 'message' );
          res.body.message.should.equal( 'User created!' );
          res.body.should.have.property( 'user' );
          res.body.should.have.property( 'accessHash' );
          res.body.should.have.property( 'accessLevel' );

          res.body.user.should.have.property( '_id' );
          userId = res.body.user["_id"];

          accessHash = res.body.accessHash;

          done();
        } );

    } );
  });

  describe('Timezones API', function () {

    it('User should be able to list timezones', function ( done ) {
        request.get('/timezones/' + accessHash)
          .set('Accept', 'application/json')
          .expect(200)
          .end(function ( err, res ) {
            if ( err ) {
              return done( err );
            }

            res.body.should.have.property('status');
            res.body.status.should.equal('OK');
            res.body.should.have.property('data');

            done();
          });
    });

    it('User should be able to create a timezone', function ( done ) {
      request.post('/timezones')
        .set('Accept', 'application/json')
        .set( 'Content-Type', 'application/json' )
        .expect(200)
        .send(JSON.stringify({
          "name": "testingTimeZone" + Math.floor((Math.random() * 1000) + 1 ).toString(),
          "nameOfACity": "London",
          "diffToGMT": Math.floor((Math.random() * 10) + 1 ).toString(),
          "userLogin": userLogin,
          "token": accessHash
        }))
        .end(function ( err, res ) {
          if ( err ) {
            return done( err );
          }

          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.should.have.property('timezone');
          res.body.timezone.should.have.property('_id');
          res.body.message.should.equal('Timezone created!');
          res.body.status.should.equal('OK');

          timezoneId = res.body.timezone["_id"];
          done();
        });
    });

    it('User should be able to update timezone details', function ( done ) {
      request.put('/timezones/' + timezoneId)
        .set('Accept', 'application/json')
        .set( 'Content-Type', 'application/json' )
        .expect(200)
        .send(JSON.stringify({
          "token": accessHash,
          "nameOfACity": "Las Vegas"
        }))
        .end(function ( err, res ) {
          if ( err ) {
            return done( err );
          }

          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.should.have.property('timezone');
          res.body.timezone.should.have.property('_id');
          res.body.message.should.equal('Timezone updated!');
          res.body.status.should.equal('OK');

          done();
        });
    });

    it('User should be able to get information about 1 specific timezone', function ( done ) {
      request.get('/timezones/' + timezoneId + '/' + accessHash)
        .set( 'Content-Type', 'application/json' )
        .expect(200)
        .end(function ( err, res ) {
          if ( err ) {
            return done( err );
          }

          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.should.have.property('timezone');
          res.body.timezone.should.have.property('_id');
          res.body.status.should.equal('OK');
          res.body.message.should.equal('OK, authorised.');

          done();
        });
    });

    it('User should be able to remove a timezone,', function ( done ) {
      request.del('/timezones/' + timezoneId)
        .set( 'Content-Type', 'application/json' )
        .set( 'Accept', 'application/json' )
        .expect(200)
        .send(JSON.stringify({
          "token": accessHash
        }))
        .end(function ( err, res ) {
          if ( err ) {
            return done( err );
          }

          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.status.should.equal('OK');
          res.body.message.should.equal('Successfully deleted');

          done();
        });
    });

    describe('Admin API', function () {
      it('App admin should be able to upgrade user permission level to admin', function ( done ) {
        request.put('/us/' + userId)
          .set( 'Content-Type', 'application/json' )
          .set( 'Accept', 'application/json' )
          .expect(200)
          .send(JSON.stringify({
            "accessLevel": "admin"
          }))
          .end(function ( err, res ) {
            if ( err ) {
              return done( err );
            }

            res.body.should.have.property('status');
            res.body.should.have.property('message');
            res.body.should.have.property('user');
            res.body.user.should.have.property('accessLevel');
            res.body.status.should.equal('OK');
            res.body.message.should.equal('User updated!');
            res.body.user.accessLevel.should.equal('admin');

            done();
          });
      });
    });

    describe('Users - remaining CRUD', function () {

      it('Admin should be able to change user\' data', function ( done ) {
        request
          .put( '/users/' + userId)
          .set( 'Content-Type', 'application/json' )
          .set( 'Accept', 'application/json' )
          .expect(200)
          .send(JSON.stringify({
            "firstName": "my very special name"
          }))
          .end(function ( err, res ) {
            if ( err ) {
              return done( err );
            }

            res.body.should.have.property('status');
            res.body.should.have.property('message');
            res.body.should.have.property('user');
            res.body.user.should.have.property('firstName');
            res.body.status.should.equal('OK');
            res.body.message.should.equal('User updated!');
            res.body.user.firstName.should.equal( "my very special name" );

            done();
          });
      });

      it('Admin should be able to get list of all users', function ( done ) {
        request
          .get('/users/fetch/' + accessHash)
          .set( 'Accept', 'application/json' )
          .expect(200)
          .end(function ( err, res ) {
            if ( err ) {
              return done( err );
            }

            res.body.should.have.property('status');
            res.body.should.have.property('message');
            res.body.should.have.property('data');
            res.body.status.should.equal('OK');
            res.body.message.should.equal('OK, authorised.');

            done();
          });

      });

      it('Admin should be able to get info about 1 specific user', function (  ) {
        request
          .get('/users/' + userId)
          .set( 'Accept', 'application/json' )
          .expect(200)
          .end(function ( err, res ) {
            if ( err ) {
              return done( err );
            }

            res.body.should.have.property('_id');
            res.body.should.have.property('accessLevel');
            res.body.accessLevel.shoul.equal('admin');

            done();
          });
      })
    });
  })

});